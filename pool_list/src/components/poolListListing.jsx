import React from "react";

const PoolListListing = ({ poolList }) => {
  return (
    <div>
      <h1>Pool List</h1>
      <ul>
        {poolList.map((pool) => (
          <li key={pool.id}>{pool.name}</li>
        ))}
      </ul>
    </div>
  );
}

export default PoolListListing;
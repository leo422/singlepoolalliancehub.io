const path = require('path');

module.exports = {
  entry: './src/index.js',
  output: {
    filename: 'scripts/bundle.js',
    path: path.resolve(__dirname, '../public'),
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env', '@babel/preset-react'],
          },
        },
      }, {
				test: /\.css$/i,
				use: ["style-loader", "css-loader"],
			  },
    ],
  },

  mode: 'production',
  experiments: {
      asyncWebAssembly: true,
      topLevelAwait: true,
      layers: true // optional, with some bundlers/frameworks it doesn't work without
      }
};

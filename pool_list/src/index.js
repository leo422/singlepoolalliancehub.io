import React from 'react';
import ReactDOM from 'react-dom';
import { Lucid } from 'lucid-cardano';
import PoolList from './components/PoolList.jsx';

const App = () => {
  return (
    <div>
      <PoolList />
    </div>
  );
};

ReactDOM.render(<App />, document.getElementById('poolList'));

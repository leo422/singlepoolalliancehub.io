import React from "react";
import { useEffect, useState } from "react";
import { Lucid , Kupmios, Blockfrost} from "lucid-cardano";
import WalletPicker from "./WalletPicker";
import "./poolList.css"
import { ToastContainer, toast } from 'react-toastify';

import './ReactToastify.css';
const poolsPerPage = 10;

async function getPoolInfo(poolId) {

    try{
    const json = await (await fetch(`https://js.cexplorer.io/api-static/pool/${poolId}.json`)).json()
    return json.data
    }catch(e){
        return undefined
    }
}


const PoolList = () => {
    const [poolList, setPoolList] = useState([]);
    const [sortBy, setSortBy] = useState(null);
    const [sortOrder, setSortOrder] = useState("asc");
    const [page, setPage] = useState(0);
    const [search, setSearch] = useState("");
    const [walletPickerOpen, setWalletPickerOpen] = useState(false);
    const [selectedPool, setSelectedPool] = useState(null);

    
    const delegateWallet = async (wallet) => {
        try{
        const api = await window.cardano[wallet].enable()
        //const provider =   new  Kupmios( "https://kupo-mainnet-wmalletmainnet-c8be04.us1.demeter.run" ,  "wss://ogmios-wmalletmainnet-c8be04.us1.demeter.run")
        const provider =   new  Blockfrost( "https://passthrough.broclan.io" ,  "mainnet")
       
        const lucid = await Lucid.new(provider , "Mainnet" );
        lucid.selectWallet(api)
        const currnetDelegation= await provider.getDelegation(await lucid.wallet.rewardAddress()) ;
        const tx =  lucid.newTx()
        if (currnetDelegation.poolId === null) {
          tx.registerStake(await lucid.wallet.rewardAddress()) 
        }  
        tx.delegateTo(await lucid.wallet.rewardAddress() , selectedPool)
        const delegationTx = await tx.complete();
        const signature = await api.signTx(delegationTx.toString());
        const completedTx = await delegationTx.assemble([signature]).complete();
        const txId = await api.submitTx(completedTx.toString());
       // console.log(txId)
        console.log(wallet)
        toast("Delegating to " + selectedPool + " successful, txId:" + txId, { position: "bottom-center" })
      }catch(e){
        console.log(e)
        toast.error("Error: " + JSON.stringify(e) , { position: "bottom-center" })
      } 
    };

    const handleDelegate = (pool) => {
        setSelectedPool(pool);
        setWalletPickerOpen(true);
    };
    const handleSort = (field) => {
        if (sortBy === field) {
          setSortOrder(sortOrder === "asc" ? "desc" : "asc");
        } else {
          setSortBy(field);
          setSortOrder("desc");
        }
        setPage(0);
      };

    const sortedData = sortBy
    ? poolList.sort((a, b) => {
        const aValue = sortBy === "pledge" ? Number(a[sortBy]) : a[sortBy];
        const bValue =  sortBy === "pledge" ? Number(b[sortBy]) : b[sortBy];
        if (aValue < bValue) return sortOrder === "asc" ? -1 : 1;
        if (aValue > bValue) return sortOrder === "asc" ? 1 : -1;
        return 0;
      })
    : poolList;
      
    const filteredData = search
    ? sortedData.filter((pool) => pool.name.toLowerCase().includes(search.toLowerCase()))
    : sortedData;

    const formatedAmount = (amount) => {
        const ADA = (amount / 1000000) 
        return ADA > 1000000 ? (ADA / 1000000).toFixed(2) + "M₳" : ADA > 1000 ? (ADA / 1000).toFixed(2) + "K₳" : ADA.toFixed(2) + "₳"    
    }

    const handleSearch = (event) => {
        setSearch(event.target.value);
        };

    useEffect(() => {
        fetch("https://raw.githubusercontent.com/SinglePoolAlliance/Registration/master/registry.json")
        .then((response) => response.json())
        .then((data) => {
            fetch("https://js.cexplorer.io/api-static/pool/hex2bech.json").then((response) => response.json()).then((hexToBench) => {

            const detailedData = data.map(async (pool) => {
                const poolInfo = await getPoolInfo(hexToBench.data[pool.poolId])
                return  poolInfo
                });
            // resolve all promises and set the state
            Promise.all(detailedData).then(detailedData => {
                    // filter out undefined values and duplicates, then shuffle

                

                    detailedData = detailedData.filter((pool) => ( pool !== undefined )).filter((pool, index, self) =>
                    index === self.findIndex((p) => (
                        p.pool_id === pool.pool_id
                    ))
                    ).sort(() => Math.random() - 0.5);

                    setPoolList(detailedData);
            });
        });
     });
    }, []);
 
  const openCexplorer = (poolId) => {
    if (poolId !== undefined)
    window.open("https://cexplorer.io/pool/" + poolId, "_blank");
    else 
    window.open("https://cexplorer.io/" , "_blank");
  };
  const arrow = (field) => {
    if (sortBy === field) {
      return sortOrder === "asc" ? "▲" : "▼";
    }
    return "";
  };

 return (
    <table className="PoolList">
      {walletPickerOpen && <WalletPicker setOpenModal={setWalletPickerOpen} operation={delegateWallet} />}
      <ToastContainer />
      <thead>
        <tr>
          <th className="NameTitle" onClick={() => handleSort("name")}>Name {arrow("name")}</th>
          <th className="Title" onClick={() => handleSort("saturation")}>Active Stake {arrow("saturation")}</th>
          <th className="Title" onClick={() => handleSort("blocks_lifetime")}>Blocks {arrow("blocks_lifetime")}</th> 

          <th className="Title" onClick={() => handleSort("pledge")}>Pledge {arrow("pledge")}</th>    
          <th className="Title" onClick={() => handleSort("tax_ratio")}>margin {arrow("tax_ratio")}</th>      
          <th className="Title" onClick={() => handleSort("tax_fix")}>fixed fee {arrow("tax_fix")}</th>
          {/* add a text input  */}
          <th className="SearchTitle"><input className="Search" onChange={handleSearch}></input>    </th> 
           

        </tr>
      </thead>
      <tbody>
        {filteredData.map((item, index) => (
          index >= page * poolsPerPage && index < (page + 1) * poolsPerPage &&
          <tr key={index}>
            <td >{item.name}</td>
            <td>{formatedAmount(item.stake_active)}({(item.saturation*100).toFixed(2)}%)     <div className="stake-bar">
                <div className="stake-bar-fill" style={{ width: (item.saturation * 100) + `%` }} /></div>  
            </td> 
            <td>{item.blocks_lifetime}</td>
            <td>{formatedAmount(item.pledge)}</td>
            <td>{Number(item.tax_ratio).toFixed(2)}%</td>
            <td>{item.tax_fix/1_000_000}₳</td>
            <td className="ButtonsRow">
                <button  onClick={() => handleDelegate(item.pool_id)}>delegate</button> <button  onClick={() => openCexplorer(item.pool_id)}>more info</button>
            </td>
          </tr>
        ))}
      </tbody>
        <tfoot>
            <tr>
                <td colSpan="6"  className="navigation">
                    <button onClick={() => setPage(page - 1)} disabled={page === 0}>Previous</button>
                    <button onClick={() => setPage(page + 1)} disabled={page === Math.floor(poolList.length / poolsPerPage)}>Next</button>
                    </td>
                    <td >
                  <div className="tableFooter" onClick={() => openCexplorer()}>
                    <span className="PoolElementAtributionText">Powered by 
                    <img className="PoolElementAtributionImg" src="assets/img/cexplorer.svg"   ></img> 
                    </span>
                    </div>
                </td>
            </tr>
        </tfoot>
    </table>
  );
    }

export default PoolList;